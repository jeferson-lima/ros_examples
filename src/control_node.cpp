#include "ros/ros.h"
#include "geometry_msgs/Twist.h"


int main(int argc, char **argv)
{
  geometry_msgs::Twist twist;
  float i;
   
  ros::init(argc, argv, "control_node");

  ros::NodeHandle n;

  ros::Publisher cmd = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 10);

  ros::Rate loop_rate(1);


while (ros::ok()) 
  {
      twist.angular.z = 2.0;
      twist.linear.x = 1.0 + i;
      i+= 0.1;

      ROS_INFO("CMD: linear x: %f, angular z %f",
                    twist.linear.x, twist.angular.z);

      cmd.publish(twist);

      ros::spinOnce();

      loop_rate.sleep();
   }
   return 0;
}
