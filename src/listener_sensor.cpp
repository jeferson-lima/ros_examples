#include "ros/ros.h"
#include "ros_examples/Sensor.h"

/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void callback(const ros_examples::Sensor::ConstPtr& msg)
{
  ROS_INFO("Sensor %s, T: %f, H: %f",
  					msg->name.c_str(), msg->temperature, msg->humidity);
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "listener_node");

  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("/sensor/value", 10, callback);

  ros::spin();

  return 0;
}
