#include "ros/ros.h"
#include "rosgraph_msgs/Clock.h"

int main(int argc, char **argv)
{
  rosgraph_msgs::Clock sys_clock;

  ros::init(argc, argv, "cpp_clock_node", ros::init_options::AnonymousName);

  ros::NodeHandle n;

  ros::Publisher clock_pub = n.advertise<rosgraph_msgs::Clock>("clock", 10);

  ros::Rate loop_rate(5);

while (ros::ok()) 
  {
      sys_clock.clock = ros::Time::now();

      clock_pub.publish(sys_clock);

      ROS_INFO("Systems time is: %f", sys_clock.clock.toSec());

      loop_rate.sleep();
   }
   return 0;
}
