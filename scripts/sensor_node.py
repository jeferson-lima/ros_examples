#!/usr/bin/env python

import rospy
from ros_examples.msg import Sensor
import numpy as np


def sensor():
    rospy.init_node('sensor_node',  anonymous=False)
    pub = rospy.Publisher('sensor/value', Sensor, queue_size=10)
    rate = rospy.Rate(1) # Hz

    while not rospy.is_shutdown():
        mySensor = Sensor()
        mySensor.name = "DHT11"
        mySensor.temperature = 15 + np.random.rand()
        mySensor.humidity = 33.34 + + np.random.rand()
        rospy.loginfo(f"{mySensor.name}, T: {mySensor.temperature}, H: {mySensor.humidity}")
        pub.publish(mySensor)
        rate.sleep()


if __name__ == '__main__':
    try:
        sensor()
    except rospy.ROSInterruptException:
        pass

