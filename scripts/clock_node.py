#!/usr/bin/env python

import rospy
from rosgraph_msgs.msg import Clock
import time

def system_clock():

    sys_clock = Clock()

    rospy.init_node("py_clock_node", anonymous=False)

    clock_pub = rospy.Publisher("clock", Clock, queue_size=10)

    loop_rate = rospy.Rate(5)

    while not rospy.is_shutdown():

        sys_clock.clock = rospy.Time.now()

        clock_pub.publish(sys_clock)

        rospy.loginfo("Systems time is: %f", sys_clock.clock.to_sec())

        loop_rate.sleep()


if __name__ == '__main__':
    try:
        system_clock()
    except rospy.ROSInterruptException:
        pass

